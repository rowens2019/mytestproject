package testprojart01.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * My test class
 * 
 * @author Royce
 *
 */
public class MyTestClass {

	public static void main(String[] args) {
		
		
		// Hey this is a test comment

		BufferedReader bin = null;
		PrintWriter pout = null;
		String instr;

		try {
			bin = new BufferedReader(new FileReader(args[0]));
			pout = new PrintWriter(args[0] + ".out");

			while ((instr = bin.readLine()) != null) {
				pout.println(instr);
			}
		} catch (Exception ex) {
			System.exit(999);
		} finally {
			try {
				bin.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (pout != null) {
				pout.flush();
				pout.close();
			}

		}

		System.out.println("Hey this was great! Ran all the way!");

	}

}
